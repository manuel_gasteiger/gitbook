---

<img align="right" width="150" height="150" src="https://www.dndbeyond.com/avatars/15720/21/1581111423-38208283.jpeg?width=150&height=150&fit=crop&quality=95&auto=webp"/>

## Silas

_Tiefling(They), Cleric_

### Stats

> - **Armor Class** 14
> - **Hit Points** 8
> - **Movement** 30 ft
> - **Passive Perception** 13
>
> |   STR   |   DEX   |   CON   |   INT   |   WIS   |   CHA   |
> | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: |
> | 13 (+1) | 13 (+1) | 11 (+0) | 11 (+0) | 17 (+3) | 18 (+4) |

### Special Items

> _None_

### Backstory - Notes

> Brother of Cassius, Best friends with Keira
> worships [Mauku](../npcs/mauku.md)
>
> ##### Human Parents
>
> afraid at first, but gods changed perception of everyone in silvermoon, Eira could see herself though
> met another tiefling (cassius) when circus came to town and got their name from him
> one day demon showed up with silas understanding him due to knowing abyssal
> Silas ran away after that and found the circus
>
> ##### Nightmares of the demon
>
> Moving Tattoos of flowers on body and the word 'home' in celestial.
>
> found aurelia and convinced her to join the circus and has big crush on her
>
> ##### Tattooist
>
> Tattoos with celestial ink move. Taught by an aasimar and has a bit of supply left from her.
>
> The details of the deal -> I can make up by myself
> (milennia ago, but mother burned all the evidence and adventure stuff because she didn't want that fate and have a normal life)
>
> ##### Sorcerer's blood
>
> instead of cleric spells sometimes randomly casts sorcerer spells
>
> ---
>
> **NOTE:**
> the character represents transgenderism:
> when you're trans -> you have an assigned gender at birth
> same with silas -> assigned human birth, but completely different things
> "noo this child can't have horns, we pray really hard so it's normal"
>
> ---

### Backstory

> ##### EIRA "The one made of snow and cold"
>
> Born from two humans in a little town close to the capital, in the Silver fields, deep down the vast forests a child is born.Born from two humans in a little town close to the capital, in the Silver fields, deep down the vast forests a child is born.
>
> They cry into the silent night for the first time, but their skin is blue as the sky and their eyes glow like the sun...Fear seated in the new parents hearts, thinking a curse had been performed in their baby, or maybe a fae had changed their child for a demon, and with only fear and desperation in their brains, both prayed to any gods that would hear them, with the hope of making their child human again. And like that, they fell asleep while praying.They cry into the silent night for the first time, but their skin is blue as the sky and their eyes glow like the sun...Fear seated in the new parents hearts, thinking a curse had been performed in their baby, or maybe a fae had changed their child for a demon, and with only fear and desperation in their brains, both prayed to any gods that would hear them, with the hope of making their child human again. And like that, they fell asleep while praying.
>
> Soon the morning came by, and both parents awakened, fear still in hand. But as their layed eyes in the newborn, the child had lost each and every single demonic trait. No longer blue skin awaited them, instead a warm and candid olive tone covered their daughter's expressions, their eyes now brown like the earth and leaves when fall came.Soon the morning came by, and both parents awakened, fear still in hand. But as their layed eyes in the newborn, the child had lost each and every single demonic trait. No longer blue skin awaited them, instead a warm and candid olive tone covered their daughter's expressions, their eyes now brown like the earth and leaves when fall came.
>
> And they thanked the gods, and eventually, as the years went by, both forgot that night, and the demon child that they now both called Eira.And they thanked the gods, and eventually, as the years went by, both forgot that night, and the demon child that they now both called Eira.
> But little did they know that what their god did was far from converting the infant tiefling into a human one, but instead set a strong spell into everyone in SilverMoon, to make them see the child as human. And neither did they know, even a spell that strong couldn’t keep the entire truth away from the young Eira, who could keep seeing her tail and blue hands while looking down, but never looking into the mirror.But little did they know that what their god did was far from converting the infant tiefling into a human one, but instead set a strong spell into everyone in SilverMoon, to make them see the child as human. And neither did they know, even a spell that strong couldn’t keep the entire truth away from the young Eira, who could keep seeing her tail and blue hands while looking down, but never looking into the mirror.
>
> Like that, the days passed by, and Eira grew as a human child, studying languages and helping in their parents inn, or helping to take care of their younger brother Glenn who loved to follow her around. And like that, Eira became thirteen, and with that, a circus setted themselves between towns, next to Eira’s home forest. Soon, the streets were filled with pamphlets and screams announcing the carnival.Like that, the days passed by, and Eira grew as a human child, studying languages and helping in their parents inn, or helping to take care of their younger brother Glenn who loved to follow her around. And like that, Eira became thirteen, and with that, a circus setted themselves between towns, next to Eira’s home forest. Soon, the streets were filled with pamphlets and screams announcing the carnival.
> The Evermore Circus where wonders and stars meet.The Evermore Circus where wonders and stars meet.
>
> ##### SILAS "The one who comes from the forest, woods"
>
> Eira had been told a million times: “Don’t get close to the circus folk, they are nothing like us, only bring problems”. Eira had been told a million times: “Don’t get close to the circus folk, they are nothing like us, only bring problems”.
>
> But as they scooted the forest limits next to the circus camps, they couldn’t do anything else but doubt, how could such a vibrant and fascinating bunch only bring problems as their parents told them? If they sparked like stars and were as colorful as Eira’s favorite flowers? There was no way this people meant wrong, they thought while getting inside the camp itself, and checking the colorful tents, forgetting their original idea of remaining unseen by them.But as they scooted the forest limits next to the circus camps, they couldn’t do anything else but doubt, how could such a vibrant and fascinating bunch only bring problems as their parents told them? If they sparked like stars and were as colorful as Eira’s favorite flowers? There was no way this people meant wrong, they thought while getting inside the camp itself, and checking the colorful tents, forgetting their original idea of remaining unseen by them.
>
> “Another one” said a low but sweet voice behind Eira, in a somehow familiar language, that they didn’t remember having ever heard or studied, but still completely understood. A little surprised, and fearful, realizing they had entered the circus folk camp without realizing, they turned around, seeing then the owner of the voice.“Another one” said a low but sweet voice behind Eira, in a somehow familiar language, that they didn’t remember having ever heard or studied, but still completely understood. A little surprised, and fearful, realizing they had entered the circus folk camp without realizing, they turned around, seeing then the owner of the voice.
> A little amazed gasp left their throat, it was the first time they had seen anyone like him, no one in SilverMoon had skin as white as his, or yellow eyes like the sun, nor horn, and even less a langid tail. A kind smile on his lips formed while waiting for a response that didn’t come, and Eira, decided in that moment, that he was the most beautiful boy they had ever seen.A little amazed gasp left their throat, it was the first time they had seen anyone like him, no one in SilverMoon had skin as white as his, or yellow eyes like the sun, nor horn, and even less a langid tail. A kind smile on his lips formed while waiting for a response that didn’t come, and Eira, decided in that moment, that he was the most beautiful boy they had ever seen.
>
> The beautiful stranger, after getting only silence as response, shaked their hands, and like it was as easy as breathing, let fire appear and change colors in their hands. Then, completely fascinated by it, got closer to the white demon boy, to see their beautiful fire. “I can show you more, if you want” said the young man to them, and no longer reminding the words of warning that they had been repeated millions of times Eira spoke back: “I would love to see more”The beautiful stranger, after getting only silence as response, shaked their hands, and like it was as easy as breathing, let fire appear and change colors in their hands. Then, completely fascinated by it, got closer to the white demon boy, to see their beautiful fire. “I can show you more, if you want” said the young man to them, and no longer reminding the words of warning that they had been repeated millions of times Eira spoke back: “I would love to see more”
>
> That way, Eira spent the rest of the afternoon with the handsome stranger, who later on, introduced himself as Cassius, meeting a bunch of other colorful circus folks, that made them laugh with no end, and captivated them with amazing stories of heroes and a god that united them all. And after getting to see a little of the magic behind the curtains, they were so fascinated by it, that the next day, they couldn't resist the temptation to come back to their camp. That way, Eira spent the rest of the afternoon with the handsome stranger, who later on, introduced himself as Cassius, meeting a bunch of other colorful circus folks, that made them laugh with no end, and captivated them with amazing stories of heroes and a god that united them all. And after getting to see a little of the magic behind the curtains, they were so fascinated by it, that the next day, they couldn't resist the temptation to come back to their camp.
>
> And like that, Eira, who got nicknamed as Silas by Cassius (since the poor child, always came from the forest, and never named themselves) became a permanent visitor during the afternoons, always following Cassius around and making them millions of question, about the [Trickster](../npcs/mauku.md), old cities and tiefling kind.And like that, Eira, who got nicknamed as Silas by Cassius (since the poor child, always came from the forest, and never named themselves) became a permanent visitor during the afternoons, always following Cassius around and making them millions of question, about the [Trickster](../npcs/mauku.md), old cities and tiefling kind.
>
> Sooner than later, the circus folk had to leave, retaking their journey from city to city. And Silas, unable to follow them, was left behind, with a tiny trinket, a name that was theirs, a secret faith, and a new friend that never left.Sooner than later, the circus folk had to leave, retaking their journey from city to city. And Silas, unable to follow them, was left behind, with a tiny trinket, a name that was theirs, a secret faith, and a new friend that never left.
>
> ##### “Fata viam invenient”- Fate will find a way
>
> Seasons went by, and one night, during Silas seventeenth winter, while their parents were visiting their uncle in the capital, they and their brother were awakened by deathly screams of their neighbors. Fear seated in their heart, as steps kept getting closer to their door and the smell of blood grew thicker in the air. Silas hid Glenn as fast as they could in a cabinet, and covered the entrance to the room he was in with their own body, with the faith that whatever it was, wouldn’t be able to get to him.
>
> The door opened before them, and soon Silas was facing a fiendish creature, cold blood runned through their veins, as their eyes met the fiends ones, a soft growl\* in an old and deep language, that Silas shouldn't have recognized, emerged from the creature. Silas fell into the ground then, as the creature left, maybe in search of a new victim, leaving them unharmed behind.
>
> _The growl was in abyssal, which Silas understands since birth without knowing why (like infernal), being the meaning of their words: "Another one"._
>
> After that the feeling in Silas that there's something evil in themselves, like a voice in the back of their head, grew stronger. But not because of guilt for being spared when so many others died, but for how the creature looked into their eyes. With respect, like Silas was just like them.
>
> They couldn’t stay, not after that, so as soon as their legs responded again, Silas rushed to their room and made a bag of their personal belongings, and the coin they had saved. Fetched Glenn from his hiding spot, and leaving through the back door into the forest, marched to the capital, to their uncles home, after explaining what happened to him, where Silas left their brother before disappearing back into the shadows of the night, with only getting as far as possible in their mind.
>
> It took them weeks traveling alone though the continent, but they eventually stumbled upon a long lost friend, far away from their birth town, and far away from Silas own demons. The Evermore Circus, now a little changed, but still full of people who once treated them kindly and who accepted them fully, and loved them as if they were part of their mismatched family, and where, after getting inside their camp by accident just like they did four years ago in their meeting, they were welcome with open arms, without a question, like those who welcome a long missed relative back home.

### Random Facts

> - FAMILY MEMBERS: Irma Baecker (mother) / Dacey Baecker (father) / Glenn Baecker (younger brother) / Amand Eden (uncle)
>
> - Silas was very popular in her town, they had multiple suitors, though she never showed actual interest.
>
> - Silas had a childhood friend named Kara, but they haven’t seen each other since she left to get married to a rich guy.
>
> - Their birth name was Eira, though they haven't used it in years.
>
> - The spell that was settled by an unknown god or entity only works in the people in SilverMoon, but in Silas only work in reflections (like a mirror, a river, etc)
>
> - They tend to avoid mirrors and any kind of reflective surface.
>
> - Silas was a given name by the circus travelers, since they always appeared out of the deep forest, and never properly introduced themselves. [Silas means wood, forest]
>
> - Once they joined the circus, they tried a lot of arts (like throwing knives, fire dance, illusionism, aerial...), and at the end they became Illiana’s tattoo apprentice.
>
> - The twins (Lavender and Thyme) love to follow Silas around, it’s rare to find them separated, though sometimes you can find them sticking to their parents too (or playing pranks on Cassius).
>
> - Silas tends to have nightmares where they see again the demon that attacked their village; sometimes they are the demon.
>
> - Silas has a chest tattoo made of flowers that represent their friends, they also have a Lavender and Thyme plants on their back, with the word home in celestial.
>
> - Silas also learned how to juggle, though they aren’t good enough to perform, they love doing it in front of children.
>
> - Silas always carried the tiny trinket Cassius gifted them before leaving when they met.
>
> - They are a follower of The Trickster god, Maukus.
>
> - Silas was the one who found Aurelia and convinced her to join their circus.
