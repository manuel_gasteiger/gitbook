
___
<img align="right" width="150" height="150" src="https://www.dndbeyond.com/avatars/15851/119/1581111423-44662613.jpeg?width=150&height=150&fit=crop&quality=95&auto=webp"/>

## Fred
*Tiefling(Male), Artificier*

### Stats

> - **Armor Class** ?
> - **Hit Points** ? 
> - **Movement** 30 ft
> - **Passive Perception** ?
>
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|8 (-1)|6 (-2)|14 (+2)|19 (+4)|12 (+1)|16 (+3)|

### Special Items

> 0. Alchemist's Supplies
> 1. Thieves' Tools

### Backstory

> Okay so, Fred was born in a cursed family, his great grandfather was a noble who had a very powerful warlock for wife and when she discovered her husband couldnt keep it in his pants, she cursed him and his bastards with demon’s blood. 
> So thats why they all like like wine/pink skinned devils with horns. 
> 
> *(Yay!! We love unresolved transgenerational trauma!!)*
> 
> The thing is, all the family believes in a prophecy that could turn them back to human, the third child of a second daughter born under a blood moon, so Fred’s mother was forced into having three children, the second of which is, you guessed it, our good ol’ Fred. 
>
> If being a middle child makes you feel invisible try being born just so the “chosen one” can be your little brother.
> Lets say Fred’s relationship with his mother is cordial in a good day, non-existiting most of the time.
> 
> But the ONE thing that his mother taught him was how to lie, and turns out our little pink tiefling became very good at that. 
> Thats why he grew up to be a pathological liar and manipulator, just like his mother. 
> In fact, Friedrich is not even his real name, not the one his mother gave him, she went with Cneo, but only her and Fred’s siblings now him like that. 
> 
> His father was a merchant, a good man that helped little Fred explore the wonders of mechanics and alchemy, even though he was never home for a long time. 
> Freds firsts attempts at becoming an artificer where repairing the toys him and his siblings would break.
>
> When he was old enough to travel alone, he grabbed his most precious invention, a watch that has illusion spells infused and can make him look human, and left. 
>
> He started working in an Alchemist Workshop in a big city. 
> There he learned a lot and met Olivia a girl who he fell very hard for. 
> She was the first person she could trust enough to start telling her the truth and she live him for who he was and not who he pretended to be.
> 
> One day Fred discovered his mentor worked for criminal organizations, selling machinery, alchemical components and all sort of gadgets for shady businesses, and being a son of his mother, he wanted part of the job. 
> He started working as a mercenary of sorts, a spy skilled in the art of deception and illusion. 
> 
> Everything worked out for a couple of years, business was good, he had Olivia and not a single of his clients knew his real identity.
> Thats until one tragic night, while Olivia and Fred where cleaning out the workshop, a mysterious man entered the front of the shop to talk with Fred’s mentor and after arguing for a while, he blew the whole place up.
> Killing everyone inside except for the one tiefling no one knew was hidding under an illusion. 
> 
> Fred lost everything that night, his life, his love, his ilusión gear, his mentor, so he has been moving around,  surviving and building back his items since then
