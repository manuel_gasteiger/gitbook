
___
<img align="right" width="150" height="150" src="https://www.dndbeyond.com/avatars/14982/293/1581111423-42571217.jpeg?width=150&height=150&fit=crop&quality=95&auto=webp"/>

## Arius
*Tiefling(Male), Druid*

### Stats

> - **Armor Class** 15
> - **Hit Points** ? 
> - **Movement** 30 ft
> - **Passive Perception** 19?
>
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|11 (+?)|13 (+?)|12 (+?)|14 (+?)|17 (+?)|18 (+?)|

### Special Items
> *None*

### Open Questions
> - why did he smuggle drugs?
> - what's the meaning of *“the last real one”*

### Backstory - Notes
> - from a wealthy household with a tiefling mom (Toril) and a human mom (Selora)
> - bookworm and obsessed with stars and nature
> - traveled together a lot
> - left his home at 21 to see the world, had a good time overall
> - while smuggling drugs for the thieves guild he was picked up and sent to jail for a week
> - there he met Luna, a human barbarian (played by other campaign by ben's bf - very overprotective, kind of bodyguard friend)
> - they traveled together for 4 years, after which they parted but stayed in touch
> - he received a letter from his moms, who received messages claiming Arius was *“the last real one”*

### Backstory

> Arius is born in Nightborn and grows up in a relatively wealthy household. His mom “Toril” is a bright blue tiefling with white eyes and his other mom “Selora” is a human woman with painted purple hair and freckles all over her body. 
> 
> The 3 of them lived in the middle of the city in a 3 story home. 
> 
> He has his own room which leads up to the roof. This is where you would find him at any point of the night if he was not asleep. 
> 
> During the day he would run around in the forest and read books about far off lands and dreams he would hope to fulfill one day.
> 
> His live was quite normal for being a light green Tiefling having two moms and being obsessed with stars and nature from a young age. 
> 
> He always loved to travel with his moms and was able to see some parts of the world . 
> 
> he got obsessed with seeing the world and started to plan his leave once he was 21. 
> This is what he did. 
> He said goodbye to his moms after a long loving hug and so many kisses he went on his way.
> 
> traveling many ways he set off to see the world. 
> He got many jobs to pay his way through the many inns he stayed in. 
> Some jobs were better than others and some people were nicer than others but overall he had a good time traveling. 
> 
> He kept a journal off all the palaces he went to and all the people he met.
> Along the way he met luna, a human fighter and they became best friends by meeting in a jail cell. 
> 
> Arius had done a job for some group of thieves - nothing too bad if you compare to things he could be doing. 
> He had just smuggled some drugs through a city. 
> which was not appreciated by the crown guard. 
> leaving Arius in the Jail of a week.
> 
> Luna was amazing; she was a barbarian with a heart of gold. 
> while in the cell they became great friends. 
> luna got out a couple days earlier than Auris leaving him alone in the cell. 
> After he sat out his time he did not expect luna to be waiting for him. 
> but she took him under her wing and so they traveled together for a year or 4 until luna had found her destination. 
> there they parted ways promising to keep in touch.
> Just after Luna left him to go on her own journey he received a letter from his mothers.
> 
> > “Dear Arius,
> > we know you are most likely on your journeys far away. 
> > But we have to ask you a favor. 
> > Lately there have been letters sent to us. 
> > We don’t know from who but whoever it has been asking for you saying “you are the last real one” we don't know what this means as you have not been home in a while. 
> > we would like to find out who this is. Is there any way you could help us darling?
> > We love you darling if you want you can always come home well be here.
> >
> > Kisses, your moms.”
 
### Random Facts:
> - Arius was born a girl but has always been a boy. 
> - This was never a problem at home. 
> - This is why Arius always wears a binder. (or a dnd like thing for it.) 
> - Arius has multiple tattoos composed of constellations, bees and mushrooms. 
> - all set by one of his mothers.
> - Arius keeps his journal so he can write a book when he goes home.

### Art

> ![AriusMom](art/arius_mom.png)