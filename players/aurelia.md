
___
<img align="right" width="150" height="150" src="./art/aurelia.png"/>

## Aurelia Leandria Targana
*Tiefling(Female), Bard/Monk(?)*

### Stats

> - **Armor Class** ?
> - **Hit Points** ? 
> - **Movement** 30 ft
> - **Passive Perception** ?
>
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|15 (+2)|13 (+3)|12 (+1)|8 (-1)|10 (+0)|16 (+3)|

### Special Items

> 0. Amulet of *Modify Memory* - able to cast *Modify Memory* on wearer if target is willing.
> 1. Yellow Ribbon - Token of her first relationship
> 2. *Bean* - giant green caterpillar plushie, her last memento of her parents.

### Backstory - Notes
> - Born to poor parents, but abducted by [Razz](../npcs/razz.md) with a loanshark scheme
> - taken to and raised at the circus as a travelling artist
> - tricked into signing a contract with him to pay back her parent's debt.
> - raised by [Lydia](../npcs/lydia.md) and taught the ways of magic and bards by her
> - after cutting off her tail, [Razz](../npcs/razz.md) is now able to access her memory directly even against her will and take control of her.
> - after running into the [Evermore Circus](../npcs/circus/circus.md), she tries to sabotage them, but gets caught by [Silas](silas.md) and joins them instead.
> - enjoys her adventurer life but knows it's only temporary until [Razz](../npcs/razz.md) finds her or makes her come back.
> - she keeps the facts about her contract and involuntary submission to [Razz](../npcs/razz.md) a secret from everyone.
> - 

### Characteristics
> 

### Relationships
> - *Annabelle*, a human girl. She was the mayor's daughter in a minor city somewhere in the empire, and she was brunette and wore a yellow dress with a yellow ribbon in her hair. They were both 12 and it was her first real friendship and her first kiss. She hid this little relationship from Razz for the month that they were stationed in that village, and when they left they never saw each other again. She still has the yellow ribbon as a token.
> - **

### Backstory

> ##### Infancy: The abduction
> She was born in some town along the coast, near a major coastal city (Aka Nicodranas ripoff) which she knows nothing except it was by the sea.
> 
> She does not really remember her parent’s faces, names or anything for that matter, only one very faded memory of them remains with her; 
> It was her 3rd or 4th birthday and they gifted her Bean, her caterpillar plushie. 
> But this memory feels very distant, like a very old photograph that's been damaged by sunlight until only shapes and silhouettes remain. 
> 
> Her parents were very very poor, and at some point they met [Razz](../npcs/razz.md), this elven showman who traveled with a carnival, who agreed to loan them a large sum of money, which they desperately needed to pay bills and feed Aurelia. 
> This man tho, *(We talk more about him later on)* was up to no good, and came back months later asking for his money back, with interest of course, which they were nowhere near to be able to pay back. 
> 
> Eventually [Razz](../npcs/razz.md) decided that if the couple was not paying with money, they were gonna be paying with something else; 
> In this cas it was baby Aurelia. 
> Against her parent’s will, she was then taken to the circus and raised there as one more travelling artist.
> 
> ##### Razz and the chavalier carnival
> See [Razz](../npcs/razz.md) for details.
>
> ##### Growing up
> She grew up relatively happy in the carnival. 
> Everyone there filled her with a false sense of security, and none really talked about their stories and their past. 
> She learnt that her biological parents were really poor and alcoholics, awful, and that they gave her in adoption to the circus just before dying. 
> The story was always vague, and neither [Razz](../npcs/razz.md) or [Lydia](../npcs/lydia.md) ever answered her questions directly. 
> She thought [Razz](../npcs/razz.md) took her out of the goodness of his heart, therefore she was grateful, and even if [Razz](../npcs/razz.md) was angry most of the time and yelled at her and was mean, she regarded him as a father figure.
> 
> When she was very little her job was to pickpocket people. 
> She would sneak between the crowd watching the show and steal wallets, bracelets, trinkets and coins; 
> Just another way [Razz](../npcs/razz.md) made money.
> 
> At 8 old [Lydia](../npcs/lydia.md) found out Aurelia could do magic, and as a bard herself she taught her how to use it and trained her in that aspect. 
> She quickly proved herself to be very talented in both dance and magic, and upgraded from pickpocketer to dancer in the main tent. 
> Once she was older, at 14 or 15, she got her own show as an aerial dancer, and became one of the main attractions of the principal act.
> She would also grow up to use her bard powers to scam people and enhance her performances; 
> Charming people, using thaumaturgy and illusions, etc. all to make more money or to get her way from trouble.
> 
> When she was 12 she broke a vase, one of [Razz](../npcs/razz.md)’s collectables. [Razz](../npcs/razz.md) was furious. 
> That's the way she got her tail cut off. 
> Once [Razz](../npcs/razz.md) got that part of her it was like signing a contract, even if she did not agree to it; 
> Razz was now connected to her, and will be as long as he has her tail, the same way [Lydia](../npcs/lydia.md), Orrag, and everyone in the circus was and is to this day. 
> He could now be in her head, see and hear through her, speak in her mind, and control her actions.
> It's not omnipresent, he is not always doing it, but it's powerful magic, and when he is in her head, he can control her like a puppet.
>
> ##### The confrontation
> When she was older (like 18 or 19) they were putting up this show in this random town. 
> One of the nights [Razz](../npcs/razz.md) came into the tent where Aurelia was staying with a little child, a human boy with brown hair and a missing tooth. 
> He threw the boy into the tent and said to Aurelia *“His parents are picking him up in a few days, take care of him in the meantime”*.
> When she asked why, he vaguely said something about his parents missing their payment.
> 
> This had Aurelias poor brain’s *(Coff coff 6 int cof cof)* wheels turning, and she had a full on breakdown connecting the dots. 
> She realized their parents were not coming back, and that they were probably gonna be dead by the morning, and started questioning the elaborate web of lies she had been raised on.
> 
> She then went into [Razz](../npcs/razz.md)’s office to confront him about all of this. 
> They had a huge fight. 
> Aurelia inquired yelling what did really happen to her parents, if she was really adopted, or if she was just kidnapped. 
> The fight got to a point where magic missiles started flying, and [Razz](../npcs/razz.md) ultimately immobilized her with spooky mind control and explained that yes, she was there by force, and that it would stay that way:
> 
> He told her her parents owed him a lot of money, and that she would have to pay it off by working for him. 
> It was not an option, she was kidnapped and she would stay kidnapped.
>
> ##### Running away
> Months after that they were in this other town. 
> They quickly realized tho, that the chevalier carnival was not the only circus staying there; 
> [The Evermore Circus](../npcs/circus/circus.md) had also put up its tent in the city. 
> The 2 bosses of both companies had a fight over who had the right to be there and bla bla bla, and nothing came out of it, so it ended up being a competition of who would be making more clientele in the week they would be there.
> 
> So obviously [Razz](../npcs/razz.md) tried sabotaging the Evermore in every possible way he could. 
> One of those tries, was sending Aurelia over the main tent with a dagger to cut off some ropes, so when the show would start the tent would collapse.
> 
> She sneaked into the main tent, did the thing and stuff, but she was caught by...[Silas](silas.md)! *chan chan chaaaaaaaan*
>
> So she gets caught, they fight a bit *‘couse you know, a thief!'*, but then [Silas](silas.md) realizes she's just a girl with quite bad bruises and scared af, and casts cure wounds on her and connects the dots about her not working at the Chavalier by her own will.
> Somehow they get her out of there and she runs away, and joins the circus gang and the rainbow tieflings!!
>
> Now the thing is, [Razz](../npcs/razz.md) still has her tail and therefore still can access her mind, and he does from time to time.
> She would get messages like *“You can run away but you'll never be free”*, *“You cannot hid from me, i own you”* or *“Enjoy your little adventure, but sooner or later, you’ll pay for it”*
> 
> So she’s obviously terrified. 
> She knows that being an adventurer is temporary and that she will not be able to escape [Razz](../npcs/razz.md) forever, that at some point she’ll have to face reality and come back to pay her debt, and that her run away time will be punished severely.
> She is aware that everything is temporary, so she tries to enjoy it while she can. 
> 
> She’s trying to collect as much money as she can, so i plan on playing her a bit greedy and selfish with money, and buying herself time by erasing her footsteps in her brain. 
> She's also quite terrified of why she is being able to be free rn, why [Razz](../npcs/razz.md) is not killing her with spooky mind control yet, and when [Razz](../npcs/razz.md) is gonna be finding her and taking her back.







