
___
<img align="right" width="150" height="150" src="https://picsum.photos/150"/>

## Siunattu Valo
*Tiefling(Male), Cleric - Life Domain*

### Stats

> - **Armor Class** ?
> - **Hit Points** ? 
> - **Movement** 30 ft
> - **Passive Perception** ?
>
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|15 (+2)|13 (+3)|12 (+1)|8 (-1)|10 (+0)|16 (+3)|

### Special Items

> *None*

### Characteristics

> ##### Traits:
> Always calm, rarely raises his voice. Only on a handful of occasions have his emotions gotten the better of him, (usually winding him up in a lot of trouble.)
> He would rather make a new friend than a new enemy.
>
> ##### Ideals:
> Redemption. There is a spark of good in everyone. 
> Chains are meant to be broken.
>
> ##### Bonds:
> Became a criminal, wanted in one country.
> Old group he was the ‘leader’ of. Religious following.

### Backstory

> Siunattu grew up poor, with kind parents. As he grew up he felt a strong pull to one deity 
> *(I haven’t chosen a deity yet so if there was a particular one that works best for you, good/chaotic/light etc, let me know and I am happy to pick them.)*
> 
> So he set off to spread the good word, encouraging people to be good, kind and share what they have.
> He gathered a following, lots of people liked his ideas and joined his group to hear more, eventually arriving in a capitol city with an amassed following of hundreds. 
> Unfortunately, the city leaders and country rulers thought that Siunattu was gathering an army to wage war.
> The rulers infiltrated the group, and somehow managed to convince someone to talk.
> Before they could come to –presumably arrest Siunattu and have him killed or imprisoned- other members of the group found out and managed to smuggle their leader out of the country.

> Siunattu has not been back since, and regrets it.


#### BACKUP CHARACTER
Siunattu’s old love-interest, member of his inner circle, Saju, who was the one forced to give up the information on Siunattu that nearly got him caught.