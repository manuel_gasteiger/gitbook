---

<img align="right" width="150" height="150" src="https://www.dndbeyond.com/avatars/27379/711/1581111423-78191243.jpeg?width=150&height=150&fit=crop&quality=95&auto=webp"/>

## Draco Ithildin

_The dragon born in starlight_

### Stats

> - **Armor Class** 13
> - **Hit Points** 30
> - **Movement** 30 ft
> - **Passive Perception** 12
>
> |   STR   |   DEX   |   CON   |   INT   |   WIS   |   CHA   |
> | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: |
> | 14 (+2) | 16 (+3) | 16 (+3) | 14 (+2) | 15 (+2) | 20 (+5) |

### Backstory

Draco was born during the twilight of a summer night, inside the safe walls of a cottage in the middle of the -----, hidden from common eye, and unknown travelers, but easily found by those few who knew of it's existence.
He had two fathers, who adored him and always took the time to teach them the mysteries of their plane. Kuu, Draco's baba and carrier, was a renowned scholar who's days consisted of writing and revising enormous tomes or looking up into the sky for hours, and sometimes some of his colleagues would show up at their door to talk and exchange books, even bringing him treats from overseas and trinkets. Draco loved sitting on the floor during the meetings, always close to his baba legs, painting what he imagined the big words of the adults meant. Kuu was slim, and elegant, always moving around like he danced or floated instead of walking around. Draco was convinced his baba must have been a prince before he had him, but Kuu was quick to softly laugh and mess his son's hair.

On the other hand there was Dror, Draco's papa, who was bigger and more muscular, with a beautiful dark grayish skin filled with white scars, short hair that made him look like he just woke up, and two bumps on his head that he always told Draco used to be a pair of horns; like those a goat had. His papa was in many ways very different from Draco's other parents, for example he would pass the days outside taking care of plants or collecting herbs that he later would turn into salves, medicine or even potions. Either to sell to a small merchant guild, or to use around the house. He liked to carry Draco on his shoulders and show him how to greet the trees before taking anything from them. If he was well behaved he would tell him stories of forgotten heroes and ancient evils, and sometimes, on special occasions, when he was particularly happy, he would sing songs in a broken Elvish to him.

Draco loved his parents, so very much, and so, the years went by. Peacefully, warm, filled with laughter and love.

One night though, on a particularly rainy night, the day before the spring equinox, something changed the little boy's life forever.

He remembers waking up to thunder, and being very afraid of it, but deciding to stay in bed because no big boy like him would run to his parents by a mere thunder. He remembers the voice of his papa, angry whispers, a loud noise and then complete silence again. Draco covered himself even tighter in his fluffy covers, and repeated his baba's lullaby in a quiet voice, like a mantra to keep him safe from the storm, till he eventually fell asleep.
An unknown noise woke him up the next morning, and Draco, excited to see what kind of fun his parents prepared for him on the celebration of the equinox, rushed down the creaky stairs. But what awaited him downstairs wasn't his parents, nor the smell of sweet spring tarts, but a completely disheveled living room, an unknown tiefling woman kneeling, looking down at what seemed like a big claw scratch. A noise escaped the child, making the woman turn around and lock eyes with the shaking kid, making the little kid shake a bit. He hadn't met an adult without one of his parents before. "It's okay little one, it's okay, I was your father's friend... You are safe now" he remembers her saying as she approached him, she looked exhausted, like she had been awake for days trying to find something. "I'm so sorry little one, whatever got here before me must have…" a big hand touched his shoulder, and without breaking eye contact she added "They are gone".

He remembers little of the following instances.
He remembers the woman looking around and throwing some of his stuff in a bag. He remembers the rain hitting his skin while being carried outside the cottage, the scent of her clothes. And he remembers crying for his parents.

The following years he spent under the care of ----, the woman who had found him the morning following his parents disappearance. He didn't have anywhere to go, no one expecting him, no home. She wouldn't let him forget, the world was tough and she was adamant that he needed to curt himself. The world was against them, and they needed to be strong to turn it around.
When he reached the age of ten, she started teaching him. How to fight, how to hide in the shadows, how to be agile like a cat. He could be great, he could help them bring balance so no other tiefling had to be lost.

By thirteen life seemed, not great, but a little less harsh. He could doge most of the attacks im training, and just end up with a pair of bruises by the end of the week and was starting to fit in with the other younglings on the cult. And then, he became fourteen, and accidentally casted a spell. It wouldn't have been a great deal in another circumstance, but Draco didn't even know he had magic, at least not apart from the usual thaumaturgy every other tiefling had, and that was very far from that. No, that had seemed like a ray of light falling from the sky, and he couldn't even deny it had been him, for he had started to glow a dim light at the same time the spell had been casted. Draco had magic, but none of the adults liked it, and soon, the air shifters around the room when he comes in and people would become quieter.

He started going on missions at the age of 20, four years later than any of the other novices his age. Probably due to his weird moon magic, as one of the kids had pointed out in a whisper s little too loud. He focused instead on gaining their people's trust, by doing the cult proud, to questioning orders till it was too late.

During one of their last missions, an attack on a ship and their passengers he started questioning why. Why were they attacking a group of tiefling like him, if they were supposed to change the world for their race? Why did they share so little information on the particular endeavor? Why did no one come to find them when Ornan and the rest of the group had been hurt or died? Why did he wash away in the sea like none of it mattered?

He woke up a few days later, in another ship, a pirate one, that knew their cult work and easily snuck him into town and helped him get in contact with the cult.

What he didn't expect was to immediately be sended into another mission, nor why he suddenly was sent into what seemed like a storage for blood. What he didn't expect was that one question could be too many, and he would be left behind to die a hundred feet underground…

### Other Information & Random Facts

[“Ad astra per aspera”-Through adversity to the stars]

FAMILY:
Kuu Ithildin, Father (carrier) - Drow [Status: Unknown]
Dror*, Father - Tiefling [Status: Unknown]
*virtue name, not birth name. Meaning freedom/sparrow in elvish.

OTHER RELATIONSHIPS:
???: Caretaker, trainer. [Status: Alive]
Ornan: Cultist, companion. Friend? [Status: Dead]

- They got a dragon tattoo on his right thigh.\*
- Has a journal where he writes whatever he can remember of his parents, inside it there's a very old portrait of his parents with him.

[ DRACO'S PLAYLIST ]

-Sorcerer subclass:
https://www.gmbinder.com/share/-Mxf93v_-Yno7UHyKZJd
