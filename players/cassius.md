---

<img align="right" width="150" height="150" src="https://www.dndbeyond.com/avatars/16109/461/1581111423-41696277.jpeg?width=150&height=150&fit=crop&quality=95&auto=webp"/>

## Cassius

_Tiefling(Male), Warlock_

### Stats

> - **Armor Class** 12
> - **Hit Points** 10
> - **Movement** 30 ft
> - **Passive Perception** 11
>
> |   STR   |   DEX   |   CON   |   INT   |   WIS   |   CHA   |
> | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: |
> | 14 (+2) | 13 (+1) | 14 (+2) | 13 (+1) | 13 (+1) | 19 (+4) |

### Special Items

> 0. Ring - Tiefling Symbol with gem eyes: all cult members have it and he kept his for easier shady accecs at some places and as a reminder
> 1. Scimitar that can light up as long as he serves Mauku (no extra dmg)

### Backstory

> Velfaris was born into the same cult as his family where he was trained to be a strong future leader without any opinion or thoughts and just to follow his mother’s wishes or commands.
>
> He often went to watch the Evermore circus at night when they were in the town without anyone knowing about it because he could never meet with anyone outside of the cult.
>
> The whole concept of show and found family amazed him so much that by the age of 17 he ran away with them because he hated the cult’s goal and the idea of using him for it.
>
> His mentor became the drow fortune teller Artemis (he/him), the only one who knows who is he and why he left.
> Together they chose the name of Cassius, his clothes and the fire dancing to start a new life.
>
> There he met with [Mauku “The Trickster”](../npcs/mauku.md) and became their warlock ([Pact of the Lover](https://homebrewery.naturalcrit.com/share/jrHD_WAv3)).
>
> Before leaving Silver Moon(his hometown) for good he met with another Tiefling sneaking into their camp.
> (As Simon wrote: _“Another one”_ said a low but sweet voice, in a somehow familiar language, that they didn’t remember having ever heard or studied, but still completely understood.
>
> A little surprised, and fearful, realizing they had entered the circus folk camp without realizing, they turned around, seeing then the owner of the voice.
> The beautiful stranger, after getting only silence as response, shaked their hands, and like it was as easy as breathing, let fire appear and change colors in their hands.
>
> Then, completely fascinated by it, got closer to the white demon boy, to see their beautiful fire.
> _“I can show you more, if you want”_).
>
> Later Cassius named them [Silas](silas.md), talked about The [Trickster](../npcs/mauku.md) and left them with a trinket as a gift.
>
> At the age of 24 Artemis asked Cassius to leave the circus to find his real goals in life and because for the cult, it was already known he lives with them.
>
> Cassius and Silas spent 3 years in the Evermore together before leaving as found siblings.
>
> In his last half year at the circus he met with Keira the monk in a tavern and after months of flirting they became a couple, and because Cassius had no goals he left to company his lover.

### Important NPCs

> [Morrigan (Mother)](../npcs/morrigan.md)

### Art

> ![Cassius Sparkly](art/cassius_sparkly.jpg)
