
___
<img align="right" width="150" height="150" src="https://www.dndbeyond.com/avatars/14602/730/1581111423-41369568.jpeg?width=150&height=150&fit=crop&quality=95&auto=webp"/>

## Worm
*Tiefling(Female), Barbarian*

### Stats

> - **Armor Class** 13
> - **Hit Points** 14
> - **Movement** 30 ft
> - **Passive Perception** 10
>
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|18 (+4)|13 (+1)|14 (+2)|12 (+1)|10 (+0)|14 (+2)|

### Special Items

> *None*

### Important NPCs

> - [Chai Park (Mother)](../npcs/chai_park.md)
> - [Storm Park (Father)](../npcs/storm_park.md)

### Backstory

> Being completely honest, Worm doesn’t know where they came from. 
> Sure they know where they grew up and who raised them, but beyond that, they don’t know.
> Worm’s parents are proud owners of Park Pastries in Nicodranas in the Menagerie coast. 
> One day when their parents were out to pick up a delivery of supplies in the early morning, they came across a basket with a squirming little red baby tiefling inside.
> Feeling obligated to help, they picked up the little child and took them back to their bakery. 
> Chai and Storm Park were planning on starting a family at some point, but they had been fairly busy with attempting to get their bakery up and running and they had been pretty distracted. 
> 
> A child popping up at the side of the road just seemed like fate and they decided to keep the child and name them Worm.
> Growing up, Worm had quite a few friends. They would spend their days roaming around the city doing whatever little kids do. 
> They were having so much fun and were the perfect child for Chai and Storm. 
> As they grew older, though, things slowly began to change. 
> Worm began to become angry at even the smallest things. 
> 
> Someone cutting in front of them, a bird on a post, the waves on the shore coming up too high. 
> They didn’t know how to handle this new found anger and began to lash out at people. 
> They would scream, yell, punch, fight. Anything to get rid of this strange rage inside them. 
> Worm began to find themselves in more trouble than the normal teenager would. 
> Worm didn’t know how to control what was going on with them, and being honest, it scared them so much.
> Their parents were not sure what to do with Worm for a while. 
> 
> They didn’t know if this behavior was a result of puberty or if it was something with their heritage. 
> It took a little while, but Chai and Storm finally figured out how to help Worm out. 
> Worm was put on heavy lifting duty. 
> They would carry the deliveries of heavy flour and sugar bags from carts to the inside of the bakery. 
> They began to spend their days working inside the bakery, kneading bread, mixing thick doughs, and helping out in general. 
> And when they had no work to do in the bakery, they would work out. 
> It helped. 
> 
> There was something so incredible about being able to transform their rage into beautiful baked goods or into making their body stronger.
> That is how Worm spent the last couple of years, but lately Worm has been wondering what the world is like outside of Nicodranas. 
> After all, they had never left the city. 
> 
> So, at the humble age of 21, Worm sets off to explore the rest of the world, making a promise to their parents to stay safe, avoid trouble, and to bring back as many interesting ingredients for testing out new recipes.

