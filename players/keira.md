
___
<img align="right" width="150" height="150" src="https://www.dndbeyond.com/avatars/14618/371/1581111423-41443295.jpeg?width=150&height=150&fit=crop&quality=95&auto=webp"/>

## Keira
*Tiefling(Female), Monk - Way of the Brewmaster*

### Stats

> - **Armor Class** 15
> - **Hit Points** 9 
> - **Movement** 30 ft
> - **Passive Perception** 12
>
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|12 (+1)|17 (+3)|13 (+1)|12 (+1)|14 (+2)|11 (+0)|

### Special Items

> 0. *The Brewer's Secret*
> 1. Potion of Healing (Greater)

### Backstory

> In the southern region of Zemni Fields lived a family who was renowned for the best beer in the whole region. 
> They were called Brauer and their family was blessed with 4 children, all of them human. What most people did not know though, was that Mrs. Brauer had an affair and had birthed a Tiefling girl named Keira. 
> 
> They locked her away, so that she wouldn’t bring shame to the family. 
> Little Keira grew up in that little room, only seeing her mother when she brought food in the evening. 
> Every now and then the little Tiefling snuck out of her room to watch her father and her sibling brew beer. 
> Fascinated she watched them, hidden away behind a barrel, wishing she could join them.
> The girls only friend was a rat she tamed and named Mouse. 
> 
> For years the child wanted to get a away from a family, who clearly didn’t love her until one day she finally came up with a plan that would accomplish that. 
> Keira wrote a letter and gave it to Mouse, telling him to find help. 
> When Mrs. Brauer came to her room that night to bring Keira food, Mouse managed to get out. 
> The rat ran for days, hoping to find someone to help his friend. 
> 
> He nearly got eaten by a cat, when he got saved by a half-elven woman who took the letter and read it. 
> The stranger asked Mouse kindly to bring her to the girl and the excited rat began running home, the woman following suit. 
> Several days went by since Mouse had left Keira and once again the little Tiefling had snuck out of her room to watch the brewing process. 
> This time though she heard an unfamiliar voice speak to her mother. 
> 
> *“I know you have a little girl hidden away in that big house. Release her to me or all of Zemni Fields will learn of your shame.”*
> 
> The Brauers reputation was everything to Keiras mother and so, without so much as a goodbye, she handed the little girl to the stranger. 
> Keira was terrified at first, but then she saw Mouse with the stranger her fears vanquished and without looking back, she went away with the half-elven woman. 
> 
> The strangers name was Elora and she took Keira to an old monastery. 
> There the child learned to fight, brew beer, and harness its powers to increase her abilities. 
> The nuns were her new family, who accepted her for who she was and so Keira discarded her family name “Brauer”. 
> 
> For a whole decade Keira stayed with the nuns and for her 25th birthday, the nuns gave her tattoos that would change colour depending on what beverages she drank and the powers those gave her. 
> Soon after, Keira said her goodbyes and set out into the world to perfect her brewing and fighting skills. 
> Her goal was to brew the best beer in Exandria so she could ruin her birth family’s reputation forever.

### Art
> ![LoveyDovey](art/cassius_with_keira.jpg)