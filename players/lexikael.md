---

<img align="right" width="150" height="150" src="https://www.dndbeyond.com/avatars/14722/10/1581111423-41643151.jpeg?width=150&height=150&fit=crop&quality=95&auto=webp"/>

## Lexikael Mardoon

_Tiefling(Female), Cleric of the Wildmother (Life Domain)_

### Stats

> - **Armor Class** 13
> - **Hit Points** 10
> - **Movement** 30 ft
> - **Passive Perception** 11
>
> |   STR   |   DEX   |   CON   |   INT   |   WIS   |   CHA   |
> | :-----: | :-----: | :-----: | :-----: | :-----: | :-----: |
> | 13 (+1) | 14 (+2) | 14 (+2) | 14 (+2) | 12 (+1) | 18 (+4) |

### Special Items

> _None_

### Backstory - Notes

> Spiritual Opposite of Siunatut

> Niece of her Mentor Melisenda, the Kaasa Clan leader.
> (One of three Clans that run the city.)
> Rose to status due to her talent for magic and languages.
> Found old book about a fallen angel - she performed a ritual to free him and gained warlock powers from him.
> (He was chained in Hell after killing the lovers of the goddess Sehanine to get closer to her - she banished him)

> Lexikael and her new angel buddy abandoned the city and her mentor to create an empire on their own - gained followers and raided cities.
> She was known as "Engelsschatten"
> A few yers of slaughter later a group of mercenaries killed the angel and she lost her powers.

> She was saved from execution by a firbolg woman. (Calaiope -> sister of Caduceus)

> After 2 years of prison the woman offered her freedom in exchange for taking an oath to the wildmother (commanded by wildmother herself)

> Not allowed to hurt anyone for fun - takes damge if she does.

> Emerald green eyes after accepting the oath, wildmother can see through them.

> Mercenary? - After prison she wanders around trying to get a new life, away from the one she had.
> Looking for job opportunities that allow her to user her healing powers.

> Own city they never attacked

### Backstory

> Lexikeal Kaasa is the niece of Melisenda Kaasa, the head of the Kaasa Clan, one of the three leading Clans of Fallshell.
> The other two Clans are the Blight Clan and the Sarroux Clan. Even if they hate each other, they run the city together, but it works and everyone gets their money.
>
> Lexikeal’s parents Heva (mother) and Vassilis (father) Kaasa did not liked the city very much, so they moved a little outside of Fallshell and did not get involved of the business of the Clan very much.
> When Lexikeal was six or so, they saw the potential and her passion for reading and send her to Melisenda.
> Melisenda became her Mentor and taught Lexikeal everything she knew.
>
> She was a quick learner and in a short time, she rose her status up to higher rank in the Clan.
> Melisenda saw, that Lexikeal has a great talent for magic and languages. She gave Lexikeal access to the library at any time.
>
> At the age of 23 and in a night of late study, Lexikeal found a very, very old book about a fallen Angel named Mitatron.
> She got obsessed with the story of the Angel and why he fall from Sehanine’s grace.
> After a ritual, she managed to break his chains who held him in one of the realms of hell and rose up into the material plain.
> For helping him, he gave Lexikeal great powers and she became a Warlock.
>
> Mitatron was once a high ranked member of the guard of Sehanine, the Moonweaver.
> He loved Sehanine so much; he tried to kill her lovers to get to her side.
> Sehanine abandoned him from any deity realm that exists and chained him in the realms of hell.
>
> With the help and powers of Mitatrons and Lexikeal’s natural talent, she become very fast a very powerful Warlock.
> Melisenda saw the raise of power in Lexikeal and wanted form her that she helped her to reclaim the power in the city what they lost over the years.
> Mitatron was bored about this idea and took Lexikeal with him to raise an own empire just for them.
> They got a quiet amount of new followers and raided a lot of cities and lands.
>
> Lexikeal was known as “Engelsschatten”
> After a few years of slaughter and destruction, rising a larger group of followers of Mitatron, a group of mercenaries attacked them.
> They managed to kill Mitatron after a long battle. They were about to kill Lexikeal too, but a female firbolg stopped the final blow.
> After the kill of Mitatron, Lexikeal lost all her powers.
> Lexikeal was taken in to prison.
>
> After 2 years, the woman came back and offered Lexikeal a deal to become a Cleric for the Wildmother.
> She has to except, that she has to use her new life and new powers to protect and heal people.
> Lex accepted the offer (because she is bored and the desire to learn more overtook her) and took an Oath to the Wildmother.
> After Lexikeal asked the woman why she spared her life, she just responded that the Wildmother herself told her just to spare her and nothing more.
> She took the name Mardoon after she was free.
>
> With the Oath she took, she is not allowed to hurt something just for fun or for bad intentions.
> If she does that, she takes the same amount of Hotpoint’s she deals. Also, her fire red eyes turned into emerald green.
> The Wildmother can now see through Lexikeal’s eyes.
> She got a vision / order from her new patron to travel too.

>
