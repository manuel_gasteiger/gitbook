# The Bloodkeep

> The largest city in the country - ruled by Vlad of house Drac. It's named after the blood red fortress towering above it.

> ![Bloodkeep](../art/bloodkeep.jpg)

> humans and vampires living together
> humans regularly have to donate blood at the bloodbank and receive gold in return
> vampires provide services and protection in exchange for the gold of the humans
> they can then exchange the gold for blood at the bloodbank

> there's also a separate currency for vampires: bloodcoins (a mixture of coal and blood mixed together, slightly magic)
> has the benefit that a vampire can eat a coin to last a day

#### Human Resources

Human Resources always has a human headmaster/mistress. At the moment it’s an older wealthy lady called elizabeth.
The staff is mostly human too - some elves.
They have detailled information on every human in the city and deal with complaints between the factions. The only jurisdiction they are under is the lord himself.

#### Militia HQ

Headed by the lord commander, an old gritty firbolg warrior that has the utmost respect of all vampires.
He has led many campaigns against the werewolves (who murdered his family) - all of them successful.
He is one of the few individuals exempt of the blood tax.
Still a master tactician, but getting too old for the battlefield, he took over managing the town defences and forces.
The hq both dispatches the undead, summons and the vampire guards - they do all have their own main buildings though.

#### Fangs and Bites Investigation

Fangs and Bites Investigations are the ones handling and investigating all forms of nonhuman on human crime. Sometimes more.
The person in charge is a mystery.

#### The slaughterhouse

All remains of the dead are brought here to be turned into undead of different sorts.
The smell if blood is strong.
It‘s across of necromants Inc. - the hq of the necromancy guild.

Below the basement is a secret complex used by the werewolf markus. He, along with several human slaves have built a secret black market for illegal blood in the city in order to weaken the vampires.
