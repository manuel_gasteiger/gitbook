# Ylvania

> A country ruled by vampire clans.
>
> It’s neutral and does it’s best to stay off the radar. To the west it borders the sea, to the north a large desert. The south and east are bordering a high mountain range separating them from the kingdoms of men. Most of the country is covered by lush forests, turning into grassland and then steppe the further north you go.
> While it’s a desert, the climate is rather cold and often reaches sub-zero temperatures.
> Large rivers form from the smaller mountainstreams and flow into the ocean. With the exception of one flowing into the desert.
>
> ##### The Dracs
>
> The most powerful vampire clan built it‘s home city in the west next to the coast. It‘s a large gothic city spanning for miles, encompassing cathedrals, parks and even some smaller farmsteads in its defenses. The fortifications are very strong with two rings of thick tower lined walls protecting the city and the keep at its center - standing atop a natural mountain. The mountain transforms into a cliff at its peak, making the castle loom over a long drop down to the sea.
>
> At the foot of the mountain a lagoon has been closed off with massive gates and transformed into a harbor, protected from the tide.
>
> The other most visible Landmarks are the cathedral and the massive bank in the central district
>
> ##### The Vauks
>
> To the northeast a rich vampire clan called the Vauks made its home. They control the trade routes through the desert to the eastern countries being the only ones that can make the trip through.
>
> Many kingdoms that want to trade with those faraway places need to ship their goods either to the bloodport or the free city of maul, then have them make the trip through the country northeast by river or horse and hand them off to the Vauks.
>
> They live int their silver citadel, an almost chrome looking sleek structure with straight towers.
>
> Apart from their citadel they do not have many mor farmsteads in the wasteland that surrounds them.
>
> ##### The Vruul
>
> The southern mountainrange is home to many smaller villages ruled by vampire counts. They form a loose organization but often fight amongst themselves. Due to the difficult location and harsh climate they are left alone, but need to take their longboats downriver regularly to raid.
>
> They also survive by sometimes coming upon the dwarves that try to dig through the tunnels from the other side.
>
> Some of those villages align themselves with the other vampire clans for different benefits as hired mercenaries (often used for attacks that should not be traced back to them)
>
> #### The Maradi
>
> Tribe of black vampires in the desert to the north.
>
> ##### Economy
>
> The economy is blood based:
> If a vampire needs 5l of blood per month which is 179ml/day
> And a human can donate 550ml every 36 days which is 15.3ml/day.
> Under normal circumstances 12 people would be needed to feed one vampire.
