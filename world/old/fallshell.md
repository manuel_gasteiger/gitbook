## Fallshell

> A mayor city, ruled by three clans.
> 
> [Lexikael](../../players/lexikael.md) was raised here, but born a little outside the city
> 
> The three clans are:
> - *The Kaasa Clan*
> - *The Blight Clan*
> - *The Sarroux Clan*
> 
> [Melisenda](../../npcs/melisenda.md) is the current ruler of the *Kaasa Clan*, who Lexikael belongs to as well.
> 
> During the slaughter and destruction caused by Ledikael and Mitatron, Fallshell was the only city that was spared.



