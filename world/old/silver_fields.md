
## The Silver Fields

> [Silas](../../players/silas.md) is from here.
> 
> The capital is *SilverMoon*
> 
> ![SilverFields](../art/silver_fields.png)
> 
> The land is covered by vast Forests.
