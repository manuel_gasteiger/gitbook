## The Evermore Circus

![Circus](../art/circus.png)

> The Evermore Circus is a prestigious carnival moving from town to town on the old continent.
> Several of our rainbow tieflings have joined its ranks over the years.
>
> The Ringmaster is a changeling man called [Taliesin](./taliesin.md).
> 
> Everyone in the circus belongs to a race that is usually rejected.
> The only humans are people married or related to other non human species.
> 
> The members:
> - [Artemis](./artemis.md) (he/him) -Drow- Fortune teller
> - [Taliesin](./taliesin.md) (he/them) -Changeling- Ringmaster [Married to Liam]
> - [Liam](./liam.md) (he/him) -Half elf- Accountant [Married to Taliesin]
> - [Bernard](./bernard.md) (he/him) - Dragonborn - Security [Dad of the twins]
> - [Illiana](./illiana.md) (she/they) - Aasimar - Tattoo artist [Mother of the twins] 
> - [Lavender](./lavender.md) (they/them) - Half dragon born, half aasimar [Twin, looks Aasimar]
> - [Thyme](./thyme.md) (they/them) - Half dragon born, half aasimar [Twin, looks Dragonborn]
> - [Cassius](../../players/cassius.md) (he/him) - PC - Fire dancer 
> - [Silas](../../players/silas.md) (he/them) - PC - Tattoo & Juggler apprentice 
> - [T'ara](../../players/tara.md) (she/her) - PC - mermaid performance
> - [Aurelia](../../players/aurelia.md) (she/her) - PC - Aerialist
> - [Myev](./myev.md) (she/her) - Half orc -Contortionist [Dating Mara] 
> - [Mara](./mara.md) (she/her) - High elf - Knife throwing [Dating Myev]
> - [Bug](./bug.md) (they/them) -Gnome- Shopkeeper [Married to Rock and Leaf]
> - [Rock](./rock.md) (they/them) -Gnome- Shopkeeper [Married to Bug and Leaf]
> - [Leaf](./leaf.md) (they/them) -Gnome- Shopkeeper [Married to Bug and Rock]
