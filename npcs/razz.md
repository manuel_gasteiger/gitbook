## Razz

> So Razz is an old elven man, with dark hair and a Dalí mustache, and also a fucking criminal and very bad person. 
> Hes is also some kind of very powerful spellcaster, specialized in mind control.
> 
> He has this traveling circus, “the chavalier carnival”, of which he is the owner and the showman. 
> He uses this as a cover for all his mafia type evil business: mostly collecting illegal treasure, like stolen relics, exotic creatures, illegal substances or potions, etc. He says hs is a collector and is vain and avaricious.
> 
> Hes is older than anyone there, so his modus operandi is to steal babies or kids (Like he did with [Aurelia](../players/aurelia.md)) by various mens, and raise them in the circus as part of his mind hive. 
> When he gets someone to owe him money, he uses his mind control powers to get this people to sign contracts to him *(He uses contracts a lot ‘couse paperwork is evil)* in where they give off some part of themselves to him (Like their vision, ears, singing...or tail) and he’s is then able to access their mind and control them, aka creating sort of a mind hive he can control as he pleases. 
> They are then forced to work for him in the circus until their debt is payed off, witch none lives long enough to do. 
> 
> Its this complex game of manipulation, where he starts as a sweet businessman loaning you money, and before you know it you’re selling him your soul. 
> This is what happens to [Aurelia](../players/aurelia.md), her parents owed him money, so he tricks her as a kid to enter this contract and access her mind, having to work her parent’s debt off.
> 
> With this mind controlled army of circus freaks he is then able to come into towns, and steal, scam, kill and manipulate his way into getting profit, may that be scamming all the gold he can from the audience, trafficking, or just killing people off and stealing their first born
