## Melisenda

> Melisenda is the current head of the *Kaasa Clan*, who rules the city of [Fallshell](../world/old/fallshell.md) alongside clan *Blight* and clan *Sarroux*.
>
> She was the mentor of her niece [Lexikael](../players/lexikael.md) until her protegé left her aunt's care and decided to go on a rampage with her angel buddy.