## Lydia

> Lidya is an air genasi with 4 arms. 
> She has light blue skin and dark blue hair, curly and fluffy that falls to the floor. 
> She is also blind, for her eyes, where Razz took from her the day she joined the circus, although Aurelia does not know her story and she doesn't talk much about herself.
> 
> She is a bard, but her main act is knife throwing; 
> The fact that a blind lady with 4 arms is throwing knives is fairly entertaining, her show always gets a lot of traction.
>
> ![lydia](art/lydia.png)
