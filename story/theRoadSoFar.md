# The Road so far...

> Party met on the old continent in a city called Porttown - the largest haven in the country - after finding pamphlets specifically asking for Tieflings with Circus and/or fighting ability.

> ![pamphlet](art/wanted_poster.jpg)

Session 0

> In the local Tavern they were scouted by an agent and sent upstairs where they met a huge man called "Siegfried".
> He offered them an enormous sum for a job spanning six months: cross the sea and work as a traveling circus troupe in his lord's domain for the specified duration.
> They accepted and decided to start the very next day.
>
> On the docks they met another member of their party: Jinx - and set sails.
> While they stayed on the ship Aurelia met and adopted the cabin boy (whatever his name was).
> During their trip they woke up finding the ship had been boarded by some cultist looking motherfuckers - and fighting started.
> Finally on deck, they found that a demon had manifested in Jinx. Said demon had created portals that allowed the cultists to board the ship in the first place.
> During the ensuing fight, the party almost died, but Jinx managed to regain control for a few seconds and used the demonic powers to open a portal for her new friends - they escaped.
>
> The party woke up severely weakened on a set of hovering stone plates in a dark room - their equipment stowed away nearby.
> They got off and equipped themselves, upon which the vampire NAV and his two ghouls entered the room.
> Being needlessly ambiguous, he offered to prepare them for dinner, upon which a fight started.
>
> After clearing up the communication issues, NAV brought them to the banquet hall, where they were welcomed by Vlad of Drac, Isabella of Drac and their butler Siegfried.
> They received food and general information about the city, their future job and so on, while Vlad was curious to find out what happened - but were also told that their adoptive son was missing.
> With Siegfried helping them out to get around town they visit "Human Ressources" and ask around town who saw something - finding out that they had landed in the city park, but that the child was never registered and that the town guard was the first one on the scene.
> Upon visiting the police headquarters, they found out that the police report never mentioned the child, event though they found evidence that the child had landed with them (plush animal & eyewitness report)
> After questioning the two officers who filed the report and responded first (Schröder & Gerhard) they know something suspicious is going on.
> Siegfried suggests going to another institution: "Fangs & Bites Investigation" - which they do and meet up with two detectives.
> mord and roach - They are aware of most of the details, but lack evidence to make a move. (Roach is a huge black bald vampire smoking cigars, mord is a female blonde elf)
> They explain where the kid might be hidden (human quarter, harbor or slaughterhouse), but they can’t freely investigate without good reason.
> They explain the value of a 'non registered human'
> They send two of the party out to gather some more information - meeting the vampire 'Balt' - ending up in a seedy vampire bar where they try to purchase illegal blood - and manage to do so!
> The dealer directs them to the slaughterhouse if they are interested in doing more business, after which they had back to Fang's and Bites to launch an investigation into the slaughterhouse.

Session 1

> infiltrating the compound in two groups, they demanded to talk to the boss - who was out at the moment, so they talked to his right hand man.
> after unsuccessfully trying to charm him, he turned into werewolf form and attacked them.
> after a long fight, a stalemate ensued - and he let them take the kid in return for letting him get away
> going back to the hotel

Session 2

> go back to hotel - in the middle of the night NAV shows up in the room
> tries to force them to give them all "potions for their own good" - they all drink
> long rest
> next day: Siegfried shows up - roach is still investigating
> tells them they got an Invitation for dinner in the evening by Vlad
> suggests going to HR and bloodbank to register cedric
> then possibly goin to militia HQ to talk to the lord commander
> they go to HR & Bloodbank to get Cedric his seal - Aurelia is officially noted down as his mother and Keira as Co-Parent

> lord commander
> werewolf slayer in the militia HQ
> back at Hotel: Karl Storefield - outfits
> had a beautiful evening & dinner at the count's palace - with his wife & son (Herbert)

> Lauriel - enchanter extraordinaire

List of Ingredients for Flask of unlimited stuff

Session 3

> Keira & Cassius had a lovely date together on the rooftop of the Hotel with some wine.
> They danced for a bit and drank some wine, resulting in Cassius falling asleep on the rooftop.
> The next day they had their big circus performance in the opera hall in the city centre.
> They met up with the Majordomo, discussed special effects and started performing.
> The show varied greatly - Poetry, acrobatics, magic and a show fight.
> During the last part, a portal similar to the one they ended up in at the beginning of the campaign opened above them and a observer + two dretches appeared.
> They managed to kill them and close the portal before anything else managed to get through.

> They had 3\* successes
> 1 failure (Aurelia faceplanted during her act from quite the height)
> The Fight was very successful as well.

Session 4

> After completely butchering the observer the party cut it up for parts and got 4 small eyes and one big one.
> Considering their massive success the last time everyone got paid a nice sum by Siegfried.
> Having witnessed the performance and noticing the nature of the portal Herbert showed up, talking to them about the fiend and what had happened.
> The group headed out, with Cassius staying behind to flirt with Herbert, but ended up accusing him of high treason and massively pissing him off.
> With their new found riches the group decided to do some shopping - most notably Keira getting a magic flask with two cubic metres of storage.
> Back at home everyone did his stuff, read some books and went to sleep while Siegfried stayed with them as protection.
> The next day they found out that during their performance the blood bank was broken into.
> Siegfried suggested that their next step might be going to lord commander to ask about werewolves.

Session 5

> The party visited the Lord Commander a.k.a. "The Huntsman".
> After some initial problems and stabbings he took them as his "werewolf slayer cadets" and introduced them into the fine art of werewolf slaying and gave them some basic equipment to do so.
> He then asked them for help regarding finding the fugitive werewolf and head of the underground blood ring "Markus" - after narrowing it down to 3 final suspects (Lothar, Ulfric & Heinrich).
> The headed back home first to get rested, Fred finished his Steel defender and Lexikael started to copy/transcribe the commander's "The subtle art of Lycantricide", for which he had offered them 500 gold (+500 if they managed to get it published).
> Meanwhile Cassius and Silas had some personal drama when details to Cassius' personal history came to light that he had hidden from Silas.
> Keira made some Cocktails.
> The next morning they started investigating the Suspects:
> Ulfric started to trust them after Lexikael revealed that she was a cleric of his own patron, the wildmother. After some convincing he asked them to make some secret deliveries for him, which turned out to be wooden carved toys that he secretly sent to children who needed them.
> Heinrich the Librarian's shifty behavior was also uncovered to be of rather mild nature: he was a member of a secret society engaged in creating fanfiction containing the country's rulers - they left after joining said club and buying some of the merchandise.
> Nearing the end of the day the headed home - Cassius visited Karl Storefield for a new outfit,

Session 6

> killing the werewolf

Session 7

> into the bank

Session 8

> in bank vault,
> Drama ensues and Silas leaves the group
> Draco is found alongside the bodies of NPCs Carl and Simmons - with a deactivated magic portal kept in place by 5 black gems
> Lexikael heals Draco and collects the gems
> s
> s
> s
> s
> s
> s

Lexi wants:

Demons with Portal abilities
Portals in general & rituals about that (how did the vault thing work)
anything about Wachter Family
